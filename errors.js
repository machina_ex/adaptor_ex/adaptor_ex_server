/**
* adaptor:ex specific errors
*
* 
* @module errors
* @copyright Lasse Marburg 2021
* @license MIT
*/
class AdaptorError extends Error {
  constructor(...params) {
    super(...params)
    this.name = this.constructor.name

    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, AdaptorError)
    }
  }
}

/**
 * An element with the given identification already exists
 */
class DuplicateError extends AdaptorError {
  constructor(...params) {
    super(...params)
    
    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, DuplicateError)
    }
  }
}

/**
 * An element with the given identification was not found
 */
class NotFoundError extends AdaptorError {
  constructor(...params) {
    super(...params)
    
    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, NotFoundError)
    }
  }
}

/**
 * The json data provided does not validate, something is missing or the wrong type
 */
 class InvalidError extends AdaptorError {
  constructor(...params) {
    super(...params)
    
    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, InvalidError)
    }
  }
}

/**
 * The json data provided is older than the original data
 */
 class OutdatedError extends AdaptorError {
  constructor(...params) {
    super(...params)
    
    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, OutdatedError)
    }
  }
}

/**
 * A connection failed or could not be established
 */
 class ConnectionError extends AdaptorError {
  constructor(...params) {
    super(...params)
    
    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, ConnectionError)
    }
  }
}

/**
 * The maximum number of elements is reached or has been exceeded
 */
 class LimitReachedError extends AdaptorError {
  constructor(...params) {
    super(...params)
    
    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, LimitReachedError)
    }
  }
}

/**
 * The operation or Request is not allowed
 */
class ForbiddenError extends AdaptorError {
  constructor(...params) {
    super(...params)
    
    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, ForbiddenError)
    }
  }
}

module.exports = {
  AdaptorError:AdaptorError,
  DuplicateError:DuplicateError,
  InvalidError:InvalidError,
  NotFoundError:NotFoundError,
  ConnectionError:ConnectionError,
  OutdatedError:OutdatedError,
  LimitReachedError: LimitReachedError,
  ForbiddenError: ForbiddenError
}
