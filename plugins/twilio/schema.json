{
  "settings": {
    "title": "Twilio Settings",
    "type": "object",
    "description": "set the connection properties for the twilio client",
    "required": true,
    "properties": {
      "sid": {
        "type": "string",
        "required": true,
        "description": "find your twilio account SID in the twilio user console at the Dashboard in Home. Its the same for all twilio instances that share one user"
      },
      "token": {
        "type": "string",
        "required": true,
        "description": "find your twilio account token in the twilio user console at the Dashboard in Home. Its the same for all twilio instances that share one user"
      }
    }
  },
  "collections": {
    "phones": {
      "type": "object",
      "title": "Phone",
      "required":["name","settings"],
      "properties": {
        "name": {
          "type": "string",
          "pattern": "^[0-9A-Za-z_-]*$"
        },
        "settings": {
          "type": "object",
          "description": "set the connection properties for this twilio phone",
          "required": ["phone_number"],
          "properties": {
            "phone_number": {
              "type": "string",
              "title": "phone number",
              "pattern":"^\\+[1-9]\\d{1,14}$",
              "description": "your twilio phone number in e.164 format (starts with '+')"
            },
            "sid": {
              "type": "string",
              "description": "Only required if you use phones from separate twilio projects. Find your twilio account SID in the twilio user console at the Dashboard in Home. Its the same for all twilio instances that share one user"
            },
            "token": {
              "type": "string",
              "description": "Only required if you use phones from separate twilio projects. Find your twilio account token in the twilio user console at the Dashboard in Home. Its the same for all twilio instances that share one user"
            },
            "method": {
              "type": "string",
              "enum": [
                "GET",
                "POST"
              ],
              "description": "Set webhook method. Method has to match twilio webhook methods."
            },
            "webhooks": {
              "type": "object",
              "description": "Use these urls to configure this phone number in Twilio. Choose 'Webhook' for incoming calls and messages and paste the respective url. The given URL needs to be accessible from the internet.",
              "properties": {
                "call": {
                  "$ref":"#/definitions/webhooks"
                },
                "callstatus": {
                  "$ref":"#/definitions/webhooks"
                },
                "sms": {
                  "$ref":"#/definitions/webhooks"
                }
              },
              "additionalProperties": false
            },
            "level": {
              "type": "string",
              "title": "Default Level",
              "description": "Define level, that are used, once this Phone is contacted and no session registered a routing for the contacting phone number.",
              "enum":[]
            },
            "voice": {
              "type": "string",
              "enum": [
                "alice",
                "man",
                "woman"
              ]
            },
            "language": {
              "type": "string",
              "enum": [
                "da-DK",
                "de-DE",
                "en-AU",
                "en-CA",
                "en-GB",
                "en-IN",
                "en-US",
                "ca-ES",
                "es-ES",
                "es-MX",
                "fi-FI",
                "fr-CA",
                "fr-FR",
                "it-IT",
                "ja-JP",
                "ko-KR",
                "nb-NO",
                "nl-NL",
                "pl-PL",
                "pt-BR",
                "pt-PT",
                "ru-RU",
                "sv-SE",
                "zh-CN",
                "zh-HK",
                "zh-TW"
              ]
            }
          }
        }
      }
    }
  },
  "data_variables": {
    "player": {
      "phone_number": {
        "type": "string"
      }
    }
  },
  "definitions": {
    "agent": {
      "title":"Twilio Phone",
      "propertyOrder": 1,
      "type": "string",
      "required": true,
      "enum": []
    },
    "flow": {
      "type": "string",
      "content": true,
      "required": true,
      "description": "Friendly name of Studio Flow that will be executed",
      "enum": []
    },
    "priority": {
      "type": "number",
      "default": 1,
      "description": "Ignore this if there is a converstion with the same members of higher priority in a different path, prefer this otherwise. If priority is the same, latest converstion is prefered.",
      "enum": [
        0,
        1,
        2
      ],
      "isAdaptorContent": false,
      "options": {
        "enum_titles": [
          "low",
          "normal",
          "high"
        ]
      }
    },
    "languages": {
      "type": "string",
      "enum": [
        "da-DK",
        "de-DE",
        "en-AU",
        "en-CA",
        "en-GB",
        "en-IN",
        "en-US",
        "ca-ES",
        "es-ES",
        "es-MX",
        "fi-FI",
        "fr-CA",
        "fr-FR",
        "it-IT",
        "ja-JP",
        "ko-KR",
        "nb-NO",
        "nl-NL",
        "pl-PL",
        "pt-BR",
        "pt-PT",
        "ru-RU",
        "sv-SE",
        "zh-CN",
        "zh-HK",
        "zh-TW"
      ]
    }
  },
  "action_variables":{
    "onSms":{
      "text":{"type":"string"},
      "match":{"type":"string"}
    }
  },
  "actions": {
    "onSms": {
      "type": "object",
      "title": "On Incoming SMS",
      "documentation":"https://docs.adaptorex.org/basics/actions/twilio/onSms.html",
      "mode": "listen",
      "resolveAdaptorVariables":false,
      "required":["agent","from"],
      "default": {
        "if": [
        ],
        "priority": 1,
        "else": {}
      },
      "properties": {
        "agent": {
          "$ref": "#/definitions/agent"
        },
        "from": {
          "$ref": "#/definitions/chat"
        },
        "priority": {
          "$ref": "#/definitions/priority"
        },
        "if": {
          "type": "array",
          "title": "if",
          "items": {
            "oneOf": [
              {
                "title": "Contains",
                "description": "Trigger next if sms contains value or any of the values in an array",
                "type": "object",
                "required": ["contains"],
                "additionalProperties": false,
                "properties": {
                  "contains": {
                    "$ref": "#/definitions/contains"
                  },
                  "case_sensitive": {
                    "type": "boolean",
                    "format": "checkbox",
                    "title": "case sensitive"
                  },
                  "respond": {
                    "type": "array",
                    "format": "table",
                    "description": "Respond to the incoming message that matches your condition.",
                    "items": {
                      "type": "string",
                      "content": true,
                      "format": "textarea",
                      "description": "Response text",
                      "required": false
                    },
                    "default": [
                      ""
                    ]
                  },
                  "next": {
                    "$ref": "#/definitions/next"
                  }
                }
              },
              {
                "title": "Equals",
                "description": "Trigger next if sms matches value or any of the values in an array",
                "type": "object",
                "required": ["equals"],
                "additionalProperties": false,
                "properties": {
                  "equals": {
                    "$ref": "#/definitions/equals"
                  },
                  "case_sensitive": {
                    "type": "boolean",
                    "format": "checkbox",
                    "title": "case sensitive"
                  },
                  "respond": {
                    "type": "array",
                    "format": "table",
                    "description": "Respond to the incoming message that matches your condition.",
                    "items": {
                      "type": "string",
                      "format": "textarea",
                      "content": true,
                      "description": "Response text",
                      "required": false
                    },
                    "default": [
                      ""
                    ]
                  },
                  "next": {
                    "$ref": "#/definitions/next"
                  }
                }
              },
              {
                "title": "Less Than",
                "description": "Trigger next if sms is number and is less",
                "type": "object",
                "required": ["lessThan"],
                "additionalProperties": false,
                "properties": {
                  "lessThan": {
                    "$ref": "#/definitions/lessThan"
                  },
                  "respond": {
                    "type": "array",
                    "format": "table",
                    "description": "Respond to the incoming message that matches your condition.",
                    "items": {
                      "type": "string",
                      "format": "textarea",
                      "content": true,
                      "description": "antwort text",
                      "required": false
                    },
                    "default": [
                      ""
                    ]
                  },
                  "next": {
                    "$ref": "#/definitions/next"
                  }
                }
              },
              {
                "title": "Greater Than",
                "description": "Trigger next if sms is number and is greater",
                "type": "object",
                "default": {
                  "greaterThan": ""
                },
                "additionalProperties": false,
                "properties": {
                  "greaterThan":{"$ref":"#/definitions/greaterThan"},
                  "respond": {
                    "type": "array",
                    "format": "table",
                    "description": "Respond to the incoming message that matches your condition.",
                    "items": {
                      "type": "string",
                      "format": "textarea",
                      "content": true,
                      "description": "antwort text",
                      "required": false
                    },
                    "default": [
                      ""
                    ]
                  },
                  "next": {
                    "$ref": "#/definitions/next"
                  }
                }
              },
              {
                "title": "Database Query",
                "description": "define query that triggers next state if it returns something",
                "type": "object",
                "additionalProperties": false,
                "properties": {
                  "query":{"$ref":"#/definitions/find"},
                  "respond": {
                    "type": "array",
                    "format": "table",
                    "description": "Respond to the incoming message that matches your condition.",
                    "items": {
                      "type": "string",
                      "format": "textarea",
                      "content": true,
                      "description": "antwort text",
                      "required": false
                    },
                    "default": [
                      ""
                    ]
                  },
                  "next": {
                    "$ref": "#/definitions/next"
                  }
                }
              },
              {
                "title": "Regular Expression",
                "description": "define query that triggers followcue if it returns something",
                "type": "object",
                "additionalProperties": false,
                "properties": {
                  "regex":{"$ref":"#/definitions/regex"},
                  "respond": {
                    "type": "array",
                    "format": "table",
                    "description": "Respond to the incoming message that matches your condition.",
                    "items": {
                      "type": "string",
                      "format": "textarea",
                      "content": true,
                      "description": "antwort text",
                      "required": false
                    },
                    "default": [
                      ""
                    ]
                  },
                  "case_sensitive":{"type":"boolean","format":"checkbox","title":"case sensitive"},
                  "next": {
                    "$ref": "#/definitions/next"
                  }
                }
              },
              {
                "title":"Javascript function",
                "description":"If javascript function returns true. Use value as function argument.",
                "type":"object",
                "additionalProperties": false,
                "required":["javascript"],
                "properties":{
                  "javascript":{"$ref":"#/definitions/javascript"},
                  "respond": {
                    "type": "array",
                    "format": "table",
                    "description": "Respond to the incoming message that matches your condition.",
                    "items": {
                      "type": "string",
                      "format": "textarea",
                      "content": true,
                      "description": "antwort text",
                      "required": false
                    },
                    "default": [
                      ""
                    ]
                  },
                  "next":{"$ref":"#/definitions/next"}
                }
              }
            ]
          }
        },
        "else": {
          "type": "object",
          "title": "else",
          "properties": {
            "next": {
              "$ref": "#/definitions/next"
            },
            "respond": {
              "type": "array",
              "format": "table",
              "description": "Respond to the incoming message that matches your condition.",
              "items": {
                "type": "string",
                "content": true,
                "format": "textarea",
                "description": "antwort text",
                "required": false
              },
              "default": [
                ""
              ]
            },
            "loop": {
              "type": "number",
              "min": 0,
              "required": false
            }
          }
        },
        "while_idle": {"$ref":"#/definitions/while_idle"},
        "finally": {"$ref":"#/definitions/finally"}
      }
    },
    "sendSms": {
      "type": "object",
      "title": "Send SMS",
      "documentation":"https://docs.adaptorex.org/basics/actions/twilio/sendSms.html",
      "template":"{return { title:`twilio ${action.name}`, subtitle:`from ${payload.agent} to ${payload.to}`, body:[{ text: payload.text }]} }",
      "required":["agent","to","text"],
      "properties": {
        "agent": {
          "$ref": "#/definitions/agent"
        },
        "to": {
          "$ref": "#/definitions/chat"
        },
        "text": {
          "type": "string",
          "content": true,
          "format": "textarea"
        }
      }
    },
    "call": {
      "type": "object",
      "title": "Outgoing Call",
      "documentation":"https://docs.adaptorex.org/basics/actions/twilio/call.html",
      "mode": "listen",
      "resolveAdaptorVariables":false,
      "default": {
        "sayplay": [
          {
            "say": ""
          }
        ]
      },
      "required":["agent","to","sayplay"],
      "properties": {
        "agent": {
          "$ref": "#/definitions/agent"
        },
        "to": {
          "title":"To",
          "$ref": "#/definitions/chat"
        },
        "sayplay": {
          "type": "array",
          "additionalProperties": false,
          "title":"SayPlay",
          "items": {
            "title":"sayplay",
            "oneOf": [
              {
                "title": "Play Audiofile",
                "type": "object",
                "description": "audiofile to play next. Collect files in public/files and reference them from there: E.g.: myfolder/myaudio.mp3",
                "additionalProperties": false,
                "properties": {
                  "play": {
                    "$ref": "#/definitions/files"
                  }
                }
              },
              {
                "title": "Say Text",
                "type": "object",
                "description": "say a text with twilio text to speech",
                "additionalProperties": false,
                "properties": {
                  "say": {
                    "type": "string",
                    "content": true,
                    "format": "textarea",
                    "required": true
                  },
                  "voice": {
                    "type": "string",
                    "enum": [
                      "alice",
                      "man",
                      "woman"
                    ],
                    "isAdaptorContent": false
                  },
                  "language": {
                    "type": "string",
                    "enum": [
                      "da-DK",
                      "de-DE",
                      "en-AU",
                      "en-CA",
                      "en-GB",
                      "en-IN",
                      "en-US",
                      "ca-ES",
                      "es-ES",
                      "es-MX",
                      "fi-FI",
                      "fr-CA",
                      "fr-FR",
                      "it-IT",
                      "ja-JP",
                      "ko-KR",
                      "nb-NO",
                      "nl-NL",
                      "pl-PL",
                      "pt-BR",
                      "pt-PT",
                      "ru-RU",
                      "sv-SE",
                      "zh-CN",
                      "zh-HK",
                      "zh-TW"
                    ],
                    "isAdaptorContent": false
                  }
                }
              }
            ]
          }
        },
        "next": {
          "$ref": "#/definitions/next"
        },
        "failed": {
          "type": "object",
          "description": "Goto State if call ended with error",
          "properties": {
            "next": {
              "$ref": "#/definitions/next"
            }
          }
        }
      }
    },
    "onCall": {
      "type": "object",
      "title": "On Incoming Call",
      "documentation":"https://docs.adaptorex.org/basics/actions/twilio/onCall.html",
      "mode": "listen",
      "resolveAdaptorVariables":false,
      "default": {
        "priority": 1,
        "sayplay": [
          {
            "say": ""
          }
        ]
      },
      "oneOf": [
        {
          "title": "Say Text and Play Audiofiles",
          "additionalProperties": false,
          "required":["agent","from","sayplay"],
          "properties": {
            "agent": {
              "$ref": "#/definitions/agent"
            },
            "from": {
              "title":"From",
              "$ref": "#/definitions/chat"
            },
            "priority": {
              "$ref": "#/definitions/priority"
            },
            "next": {
              "$ref": "#/definitions/next"
            },
            "failed": {
              "type": "object",
              "description": "Goto State if call ended with error",
              "properties": {
                "next": {
                  "$ref": "#/definitions/next"
                }
              }
            },
            "sayplay": {
              "type": "array",
              "additionalProperties": false,
              "title":"SayPlay",
              "items": {
                "title":"sayplay",
                "oneOf": [
                  {
                    "title": "Play Audiofile",
                    "type": "object",
                    "description": "audiofile to play next. Collect files in public/files and reference them from there: E.g.: myfolder/myaudio.mp3",
                    "additionalProperties": false,
                    "properties": {
                      "play": {
                        "$ref": "#/definitions/files"
                      }
                    }
                  },
                  {
                    "title": "Say Text",
                    "type": "object",
                    "description": "say a text with twilio text to speech",
                    "additionalProperties": false,
                    "properties": {
                      "say": {
                        "type": "string",
                        "content": true,
                        "format": "textarea",
                        "required": true
                      },
                      "voice": {
                        "type": "string",
                        "enum": [
                          "alice",
                          "man",
                          "woman"
                        ],
                        "isAdaptorContent": false
                      },
                      "language": {
                        "type": "string",
                        "enum": [
                          "da-DK",
                          "de-DE",
                          "en-AU",
                          "en-CA",
                          "en-GB",
                          "en-IN",
                          "en-US",
                          "ca-ES",
                          "es-ES",
                          "es-MX",
                          "fi-FI",
                          "fr-CA",
                          "fr-FR",
                          "it-IT",
                          "ja-JP",
                          "ko-KR",
                          "nb-NO",
                          "nl-NL",
                          "pl-PL",
                          "pt-BR",
                          "pt-PT",
                          "ru-RU",
                          "sv-SE",
                          "zh-CN",
                          "zh-HK",
                          "zh-TW"
                        ],
                        "isAdaptorContent": false
                      }
                    }
                  }
                ]
              }
            }
          }
        },
        {
          "title": "Forward to Twilio Studio Flow",
          "additionalProperties": false,
          "required":["agent","from","flow"],
          "properties": {
            "agent": {
              "$ref": "#/definitions/agent"
            },
            "from": {
              "$ref": "#/definitions/chat"
            },
            "priority": {
              "$ref": "#/definitions/priority"
            },
            "flow": {
              "$ref": "#/definitions/flow"
            },
            "next": {
              "$ref": "#/definitions/next"
            }
          }
        },
        {
          "title": "Reject Call",
          "additionalProperties": false,
          "required":["agent","from","reject"],
          "properties": {
            "agent": {
              "$ref": "#/definitions/agent"
            },
            "from": {
              "$ref": "#/definitions/chat"
            },
            "priority": {
              "$ref": "#/definitions/priority"
            },
            "reject": {
              "type": "string",
              "required": true,
              "description": "do not take the call. Set a reason.",
              "enum": [
                "busy",
                "rejected"
              ],
              "isAdaptorContent": false
            },
            "next": {
              "$ref": "#/definitions/next"
            }
          }
        }
      ]
    },
    "flow": {
      "type": "object",
      "title": "Twilio Studio Flow",
      "description": "Execute a Twilio Studio Flow.",
      "documentation":"https://docs.adaptorex.org/basics/actions/twilio/flow.html",
      "template":"{return { title:`twilio ${action.name}`, subtitle:`with ${payload.agent} and ${payload.to}`, body:[{ text: `Start Flow ${payload.flow}` }]} }",
      "required":["agent","to","flow"],
      "properties": {
        "agent": {
          "$ref": "#/definitions/agent"
        },
        "to": {
          "$ref": "#/definitions/chat"
        },
        "flow": {
          "$ref": "#/definitions/flow"
        },
        "parameters": {
          "type": "object",
          "description": "forward parameters to flow execution. Access them in the flow with {{flow.data.<parameter>}}",
          "additionalProperties": {
            "type": "string"
          }
        }
      }
    }
  }
}