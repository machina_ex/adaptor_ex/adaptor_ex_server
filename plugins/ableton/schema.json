{
    "settings":{
      "type":"object",
      "title":"Ableton Live Settings",
      "description": "Start and Stop tracks, clips and scenes in your livesets.",
      "additionalProperties": false,
      "readonly": true,
      "properties":{
      }
    },
    "actions": {
      "playClip":{
        "type":"object",
        "title":"Play Clip",
        "description":"Start a single clip in the Live Set.",
        "documentation":"https://docs.adaptorex.org/basics/actions/ableton/playClip.html",
        "template":"{return { title:`ableton ${payload.liveset?.replace('ableton.livesets.', '')} ${action.name}`, body:[{text: `Play: ${payload.clip}`}]} }",
        "required":["liveset","clip"],
        "additionalProperties":false,
        "properties":{
          "liveset":{
            "$ref":"#/definitions/ableton_livesets"
          },
          "clip":{
            "$ref":"#/definitions/live_clip"
          }
        }
      },
      "playScene":{
        "type":"object",
        "title":"Play Scene",
        "description":"Launch all clips in a Scene in the Live Set.",
        "documentation":"https://docs.adaptorex.org/basics/actions/ableton/playScene.html",
        "template":"{return { title:`ableton ${payload.liveset?.replace('ableton.livesets.', '')} ${action.name}`, body:[{text: `Play: ${payload.scene}`}]} }",
        "required":["liveset","scene"],
        "additionalProperties":false,
        "properties":{
          "liveset":{
            "$ref":"#/definitions/ableton_livesets"
          },
          "scene":{
            "$ref":"#/definitions/live_scene"
          }
        }
      },
      "stopClip":{
        "type":"object",
        "title":"Stop Clip",
        "description":"Stop a clip in the live set.",
        "documentation":"https://docs.adaptorex.org/basics/actions/ableton/stopClip.html",
        "template":"{return { title:`ableton ${payload.liveset?.replace('ableton.livesets.', '')} ${action.name}`, body:[{text: `Stop: ${payload.clip}`}]} }",
        "required":["liveset","clip"],
        "additionalProperties":false,
        "properties":{
          "liveset":{
            "$ref":"#/definitions/ableton_livesets"
          },
          "clip":{
            "$ref":"#/definitions/live_clip"
          }
        }
      },
      "stopTrack":{
        "type":"object",
        "title":"Stop Track",
        "description":"Stop all clips in a track in the Live Set.",
        "documentation":"https://docs.adaptorex.org/basics/actions/ableton/stopTrack.html",
        "template":"{return { title:`ableton ${payload.liveset?.replace('ableton.livesets.', '')} ${action.name}`, body:[{text: `Stop: ${payload.track}`}]} }",
        "required":["liveset","track"],
        "additionalProperties":false,
        "properties":{
          "liveset":{
            "$ref":"#/definitions/ableton_livesets"
          },
          "track":{
            "$ref":"#/definitions/live_track"
          }
        }
      },
      "trackMix": {
        "type": "object",
        "title":"Set Track Mix",
        "description":"Change the mixer settings of a live set track.",
        "documentation":"https://docs.adaptorex.org/basics/actions/ableton/trackMix.html",
        "required":["liveset","track"],
        "additionalProperties":false,
        "properties": {
          "liveset":{
            "$ref":"#/definitions/ableton_livesets"
          },
          "track": {
            "$ref":"#/definitions/live_track"
          },
          "volume": {
            "type": "string",
            "default":"0.0",
            "description":"Value from 0.0 (off) to 1.0 (max)"
          },
          "panning": {
            "type": "string",
            "default":"0.0",
            "description":"Value from -1.0 (all left) to 1.0 (all right)"
          },
          "track_activator": {
            "type": "string",
            "default":"0",
            "description":"Activate Track with 1, deactivate with 0"
          },
          "sends": {
            "type":"object",
            "additionalProperties":{
              "type":"string",
              "default":"0.0",
              "description":"Value from 0.0 (off) to 1.0 (max)"
            },
            "title":"Sends",
            "default":{"A":"0.0","B":"0.0"},
            "properties":{
              "A":{
                "type":"string",
                "description":"Value from 0.0 (off) to 1.0 (max)"
              },
              "B":{
                "type":"string",
                "description":"Value from 0.0 (off) to 1.0 (max)"
              }
            }
          }
        }
      }
    },
    "definitions":{
      "live_clip":{
        "anyOf":[
          {
            "title":"custom",
            "type":"string"
          }
        ]
      },
      "live_track":{
        "anyOf":[
          {
            "title":"custom",
            "type":"string"
          }
        ]
      },
      "live_scene":{
        "anyOf":[
          {
            "title":"custom",
            "type":"string"
          }
        ]
      }
    },
    "collections": {
      "livesets": {
        "type": "object",
        "title": "Liveset",
        "required":["name", "settings"],
        "properties": {
          "name":{
            "type":"string"
          },
          "settings": {
            "type": "object",
            "description": "set the connection properties for this liveset",
            "required":["port", "ip", "adaptor_port"],
            "properties": {
              "port": {
                "type": "number",
                "title":"Liveset Port",
                "default": 9000,
                "description": "Port on which the liveset communicates with adaptor:ex. 0 to set random port."
              },
              "ip": {
                "type": "string",
                "title":"Liveset IP",
                "default":"127.0.0.1",
                "description": "The ip address of the computer your liveset runs on, so that adaptor:ex can send messages to it."
              },
              "adaptor_port": {
                "type": "number",
                "title":"Adaptor Port",
                "default":9001,
                "description": "Set an alternative port where the liveset sends data to adaptor:ex if its different from the port above."
              }
            }
          }
        }
      }
    }
  }