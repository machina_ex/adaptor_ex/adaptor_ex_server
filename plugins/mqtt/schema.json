{
    "settings": {
        "type": "object",
        "title": "MQTT CLIENT",
        "description": "MQTT client to publish and listen to topics",
        "additionalProperties": false,
        "readonly": false,
        "required": [
            "url"
        ],
        "properties": {
            "url": {
                "title": "Broker URL",
                "description": "MQTT Broker URL or IP address\ne.g. mqtt://test.mosquitto.org:1883",
                "type": "string",
                "minLength": 1,
                "isAdaptorContent": false
            },
            "username": {
                "title": "username",
                "description": "User to access MQTT Broker",
                "type": "string",
                "minLength": 1,
                "isAdaptorContent": false
            },
            "password": {
                "title": "password",
                "description": "Password to access protected MQTT Broker",
                "type": "string",
                "format":"password",
                "minLength": 1,
                "isAdaptorContent": false
            },
            "keepalive": {
                "title": "keepalive",
                "default":60,
                "description": "10 seconds, set to 0 to disable",
                "type": "number"
            },
            "clientId": {
                "title": "clientId",
                "description": "Set a custom client id for adaptor:ex",
                "type": "string"
            },
            "clean": {
                "title": "clean",
                "default":true,
                "description": "default is true, set to false to receive QoS 1 and 2 messages while offline",
                "type": "boolean"
            },
            "reconnectPeriod": {
                "title": "reconnectPeriod",
                "default":1000,
                "description": "milliseconds interval between two reconnection attempts. Default is 1000 milliseconds.",
                "type": "number"
            },
            "connectTimeout": {
                "title": "connectTimeout",
                "default":30000,
                "description": "time in milliseconds to wait before a CONNACK is received. Default is 30 * 1000 milliseconds.",
                "type": "number"
            },
            "resubscribe": {
                "title": "resubscribe",
                "default":true,
                "description": "set to false to disable re-subscribe functionality.",
                "type": "boolean"
            },
            "will": {
                "title": "will",
                "description": "a message that will be sent by the broker automatically when the client disconnect badly.",
                "type": "object",
                "additionalProperties":false,
                "properties":{
                    "topic":{
                        "type":"string",
                        "description":"The topic to publish"
                    },
                    "payload":{
                        "type":"string",
                        "description":"The message to publish"
                    },
                    "qos":{
                        "type":"string"
                    },
                    "retain":{
                        "type":"boolean",
                        "default":false
                    }
                }
            }
        }
    },
    "action_variables":{
        "onMQTTMessage":{
            "match":{"type":["string","number","integer"]},
            "message":{"type":["string","number","integer","object","array"]},
            "topic":{"type":"string"},
            "topic_levels":{"type":"array"},
            "topic_contains":{"type":"array"},
            "retain":{"type":"boolean"},
            "qos":{"type":"number"},
            "dup":{"type":"boolean"},
            "length":{"type":"number"}
        }
    },
    "definitions":{
        "topic_contains":{
            "type":"string",
            "title":"topic contains",
            "description":"Condition will only match if incoming topic contains this text. Allows regex syntax. See javascript string match function for details.",
            "propertyOrder":1
        }
    },
    "actions": {
        "sendMQTTMessage": {
            "description": "send a MQTT message",
            "mode": "run",
            "type": "object",
            "title": "Send MQTT Message",
            "documentation":"https://docs.adaptorex.org/basics/actions/mqtt/sendMQTTMessage.html",
            "required": [
                "topic",
                "message"
            ],
            "additionalProperties": false,
            "properties": {
                "topic": {
                    "type": "string",
                    "minLength": 1,
                    "description": "topic to send message to.",
                    "isAdaptorContent": false
                },
                "message": {
                    "description": "message payload.",
                    "type":"string"
                },
                "retain":{
                    "type":"boolean",
                    "format":"checkbox",
                    "description":"retained message are stored and republished to each client that subscribes to the topic."
                }
            }
        },
        "onMQTTMessage": {
            "description": "Cue the next state after a MQTT message on specific topic arrived.",
            "mode": "listen",
            "type": "object",
            "title": "On MQTT Message",
            "documentation":"https://docs.adaptorex.org/basics/actions/mqtt/onMQTTMessage.html",
            "required": [
                "topic"
            ],
            "additionalProperties": false,
            "resolveAdaptorVariables":false,
            "properties": {
                "topic": {
                    "type": "string",
                    "minLength": 1,
                    "description": "topic",
                    "isAdaptorContent": false
                },
                "if": {
                    "type": "array",
                    "title": "if",
                    "items": {
                        "title": "condition",
                        "oneOf": [
                            {
                                "title": "Equals",
                                "description": "If message equals the or any of the following values",
                                "type": "object",
                                "additionalProperties": false,
                                "required": [
                                    "equals"
                                ],
                                "properties": {
                                    "topic_contains":{
                                        "$ref": "#/definitions/topic_contains"
                                    },
                                    "field": {
                                        "$ref": "#/definitions/field"
                                    },
                                    "equals": {
                                        "$ref": "#/definitions/equals"
                                    },
                                    "case_sensitive": {
                                        "type": "boolean",
                                        "format": "checkbox",
                                        "title": "case sensitive"
                                    },
                                    "next": {
                                        "$ref": "#/definitions/next"
                                    }
                                }
                            },
                            {
                                "title": "Contains",
                                "description": "If message contains the or any of the following values",
                                "type": "object",
                                "additionalProperties": false,
                                "required": [
                                    "contains"
                                ],
                                "properties": {
                                    "topic_contains":{
                                        "$ref": "#/definitions/topic_contains"
                                    },
                                    "field": {
                                        "$ref": "#/definitions/field"
                                    },
                                    "contains": {
                                        "$ref": "#/definitions/contains"
                                    },
                                    "case_sensitive": {
                                        "type": "boolean",
                                        "format": "checkbox",
                                        "title": "case sensitive"
                                    },
                                    "next": {
                                        "$ref": "#/definitions/next"
                                    }
                                }
                            },
                            {
                                "title": "Less Than",
                                "description": "If message is value that is less than",
                                "type": "object",
                                "default": {
                                    "lessThan": ""
                                },
                                "additionalProperties": false,
                                "properties": {
                                    "topic_contains":{
                                        "$ref": "#/definitions/topic_contains"
                                    },
                                    "field": {
                                        "$ref": "#/definitions/field"
                                    },
                                    "lessThan": {
                                        "$ref": "#/definitions/lessThan"
                                    },
                                    "next": {
                                        "$ref": "#/definitions/next"
                                    }
                                }
                            },
                            {
                                "title": "Greater Than",
                                "description": "If message is value that is greater than",
                                "type": "object",
                                "default": {
                                    "greaterThan": ""
                                },
                                "additionalProperties": false,
                                "properties": {
                                    "topic_contains":{
                                        "$ref": "#/definitions/topic_contains"
                                    },
                                    "field": {
                                        "$ref": "#/definitions/field"
                                    },
                                    "greaterThan": {
                                        "$ref": "#/definitions/greaterThan"
                                    },
                                    "next": {
                                        "$ref": "#/definitions/next"
                                    }
                                }
                            },
                            {
                                "title": "Regular Expression",
                                "description": "If regular expression returns anything when used on incoming message",
                                "type": "object",
                                "default": {
                                    "regex": ""
                                },
                                "additionalProperties": false,
                                "properties": {
                                    "topic_contains":{
                                        "$ref": "#/definitions/topic_contains"
                                    },
                                    "field": {
                                        "$ref": "#/definitions/field"
                                    },
                                    "regex": {
                                        "$ref": "#/definitions/regex"
                                    },
                                    "case_sensitive":{
                                        "type":"boolean","format":"checkbox","title":"case sensitive"
                                    },
                                    "next": {
                                        "$ref": "#/definitions/next"
                                    }
                                }
                            },
                            {
                                "title": "Javascript function",
                                "description": "If javascript function returns true. Use value as function argument.",
                                "type": "object",
                                "default": {
                                    "javascript": ""
                                },
                                "additionalProperties": false,
                                "properties": {
                                    "topic_contains":{
                                        "$ref": "#/definitions/topic_contains"
                                    },
                                    "javascript": {
                                        "$ref": "#/definitions/javascript"
                                    },
                                    "next": {
                                        "$ref": "#/definitions/next"
                                    }
                                }
                            },
                            {
                                "title": "Database Query",
                                "description": "If database find query returns a valid document. You can use action var in query: [[action.message]]",
                                "type": "object",
                                "default": {
                                    "find": ""
                                },
                                "additionalProperties": false,
                                "properties": {
                                    "topic_contains":{
                                        "$ref": "#/definitions/topic_contains"
                                    },
                                    "find": {
                                        "$ref": "#/definitions/find"
                                    },
                                    "next": {
                                        "$ref": "#/definitions/next"
                                    }
                                }
                            }
                        ]
                    }
                },
                "else": {
                    "type": "object",
                    "properties": {
                        "next": {
                            "$ref": "#/definitions/next"
                        }
                    }
                },
                "keep_listening": {"$ref":"#/definitions/keep_listening"}
            }
        }
    }
}