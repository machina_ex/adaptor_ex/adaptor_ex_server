{
  "settings":{
    "type":"object",
    "title":"Control",
    "description":"Control this level and launch new sessions.",
    "additionalProperties":false,
    "readonly":true
  },
  "action_variables":{
    "onError":{
      "name":{"type":"string"},
      "type":{"type":"string"},
      "match":{"type":"string"},
      "message":{"type":"string"},
      "state":{
        "type":"object",
        "properties":{
          "name":{"type":"string"},
          "id":{"type":"string"}
        }
      }
    }
  },
  "actions":
  {
    "next":{
      "type":"object",
      "title":"Next",
      "description":"Goto next state immediately after all actions are executed.",
      "documentation":"https://docs.adaptorex.org/basics/actions/control/next.html",
      "required":["next"],
      "default":{"next":""},
      "additionalProperties":false,
      "properties":{
        "next":{"$ref":"#/definitions/next"}
      }
    },
    "onCue":{
      "title":"On Cue",
      "type":"object",
      "mode":"listen",
      "required":["cue","next"],
      "description":"Next state triggered by a human operator on a specified cue.",
      "documentation":"https://docs.adaptorex.org/basics/actions/control/onCue.html",
      "template":"{return { title:`control ${action.name}`, subtitle:`Click ${payload.next} on:`, body:[{ text: payload.cue, next: payload.next }]} }",
      "additionalProperties": false,
      "properties":{
        "cue":{
          "title":"Cue",
          "description":"Describe the event on which an operator should trigger the next state manually.",
          "type":"string",
          "format":"textarea"
        },
        "next":{"$ref":"#/definitions/next"}
      }
    },
    "quit":{
      "type":"null",
      "title":"Quit",
      "template":"{return { title:`Quit Level`} }",
      "description":"End all states and listeners and close the session.",
      "documentation":"https://docs.adaptorex.org/basics/actions/control/quit.html",
      "readOnly":true
    },
    "split":{
      "type":"array",
      "title":"Split Path",
      "minItems":2,
      "format":"table",
      "description":"Split your flow in paths that run independent from each other.",
      "documentation":"https://docs.adaptorex.org/basics/actions/control/split.html",
      "default":[{"name":"red"},{"name":"blue"}],
      "items":{
        "type":"object",
        "required":["name", "next"],
        "properties": {
          "name":{
            "type":"string",
            "description":"name the new path so its easier to identify it.",
            "isAdaptorContent":false
          },
          "next":{"$ref":"#/definitions/next"}
        }
      }
    },
    "join":{
      "type":"array",
      "title":"Join Path",
      "format":"table",
      "description":"cancel all listeners running on a specific path defined by a list of origin paths",
      "documentation":"https://docs.adaptorex.org/basics/actions/control/join.html",
      "default":[""],
      "items":{
        "type":"string",
        "title":"path",
        "isAdaptorContent":false
      }
    },
    "launch": {
      "type":"object",
      "description":"start a new session",
      "documentation":"https://docs.adaptorex.org/basics/actions/control/launch.html",
      "title":"Launch Session",
      "required":["level"],
      "resolveAdaptorVariables":false,
      "additionalProperties": false,
      "properties":{
        "level":{
          "type":"string",
          "description":"name of level the new session is based on",
          "isAdaptorContent":false
        },
        "name":{
          "type":"string",
          "description":"name the new session so it can be accessed and identified. Launch will fail if another session with the same name exists.",
          "isAdaptorContent":false
        },
        "reference":{
          "type":"string",
          "description":"allows you to reference the new session from inside this level.",
          "isAdaptorContent":false
        },
        "content":{
          "type":"string",
          "description":"Set session content actions will refer to. E.g. language.",
          "isAdaptorContent":false
        },
        "arguments":{
          "type":"array",
          "description":"Add values, variables or items as arguments that will be forwarded to the new session.",
          "options":{
            "disable_array_reorder":true
          },
          "default":[{"name":"","value":""}],
          "format":"table",
          "items":{
            "type":"object",
            "title":"argument",
            "format":"grid",
            "additionalProperties":false,
            "required":["name","value"],
            "options":{
              "disable_properties":true
            },
            "properties":{
              "name":{
                "type":"string"
              },
              "value":{
                "type":"string"
              }
            }
          }
        }
      }
    },
    "cancel":{
      "type":"object",
      "title":"Cancel Session",
      "description":"Cancel a remote session",
      "documentation":"https://docs.adaptorex.org/basics/actions/control/cancel.html",
      "template":"{return { title:`Cancel Session ${action.name}`, body:[{ text: payload.session}]} }",
      "isAdaptorContent":false,
      "additionalProperties": false,
      "required":["session"],
      "properties":{
        "session":{
          "type":"string",
          "description":"Session name, reference or a query to find and cancel one or more sessions."
        }
      }
    },
    "log":{
      "type":"object",
      "title":"Log Message",
      "description":"Write an info, debug or error message to the adaptor log",
      "documentation":"https://docs.adaptorex.org/basics/actions/control/log.html",
      "template":"{return { title:`control ${action.name}`, body:[{ text: `${payload.level}: ${payload.message}`}]} }",
      "required":["level","message"],
      "additionalProperties": false,
      "properties":{
        "level":{
          "type":"string",
          "enum":["error","warn","info","debug","trace"],
          "default":"info",
          "description":"Set the urgency level for the log message. Error messages are most and trace messages least important."
        },
        "message":{
          "type":"string",
          "default":"",
          "format":"textarea"
        }
      }
    },
    "onError": {
      "type":"object",
      "title":"On Error",
      "description":"Listen for errors inside this state or the session.",
      "documentation":"https://docs.adaptorex.org/basics/actions/logic/onError.html",
      "mode":"listen",
      "adaptorExecutionOrder":"before_run",
      "resolveAdaptorVariables":false,
      "default":{"from":"state", "type":"Error"},
      "additionalProperties": false,
      "properties":{
        "from":{
          "type":"string",
          "enum":["state","session"]
        },
        "type":{
          "title":"error type name",
          "description":"Match Errors of a specific type only. Select 'Error' to match any type of error. Select 'AdaptorError' to match any of the known error types from this list.",
          "type":"string",
          "enum":["Error","AdaptorError","NotFoundError","ConnectionError","DuplicateError","InvalidError","OutdatedError","ForbiddenError","LimitReachedError"]
        },
        "if":{
          "title":"if",
          "type":"array",
          "items":{
            "title":"condition",
            "oneOf":[
              {
                "title":"Equals",
                "description":"Trigger next if error message equals",
                "type":"object",
                "additionalProperties": false,
                "required":["equals","next"],
                "properties":{
                  "equals":{"$ref":"#/definitions/equals"},
                  "case_sensitive":{"type":"boolean","format":"checkbox","title":"case sensitive"},
                  "next":{"$ref":"#/definitions/next"}
                }
              },
              {
                "title":"Contains",
                "description":"Trigger next if error message contains",
                "type":"object",
                "additionalProperties": false,
                "required":["contains","next"],
                "properties":{
                  "contains":{"$ref":"#/definitions/contains"},
                  "case_sensitive":{"type":"boolean","format":"checkbox","title":"case sensitive"},
                  "next":{"$ref":"#/definitions/next"}
                }
              },
              {
                "title":"Javascript function",
                "description":"If javascript function returns. Error message is value argument.",
                "type":"object",
                "additionalProperties": false,
                "required":["javascript","next"],
                "properties":{
                  "javascript":{"$ref":"#/definitions/javascript"},
                  "next":{"$ref":"#/definitions/next"}
                }
              },
              {
                "title":"Regular Expression",
                "description":"If regular expression matches anything when used on error message",
                "type":"object",
                "additionalProperties": false,
                "required":["regex","next"],
                "properties":{
                  "regex":{"$ref":"#/definitions/regex"},
                  "case_sensitive":{"type":"boolean","format":"checkbox","title":"case sensitive"},
                  "next":{"$ref":"#/definitions/next"}
                }
              }
            ]
          }
        },
        "else": {
          "type":"object",
          "properties":{
            "next":{"$ref":"#/definitions/next"}
          }
        }
      }
    }

  }
}
